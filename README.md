# YukaZZ

YukaZZ est une api accessible via l'url : https://yukazz.herokuapp.com/ développée dans le cadre d'un cours de java professionnel.
Elle a été réalisée par Marion Bertoni et Mathilde Henry.

YukaZ communique avec l'api d'Open Food Facts disponible à l'adresse https://fr.openfoodfacts.org/data.
Elle utilise également une base de données h2 afin de sauvegarder les différents éléments.


Une documentation de l'api YukaZZ a été générée automatiquement à partir des commentaires écrits dans le code. Elle est disponible dans le dossier "doc".

### Requêtes disponibles

- **addUser?name=toto&email=toto@gmail.com**
Création d'un utilisateur avec son nom et son mail (le mail doit être unique, cet élément sert plus tard à identifier l'utilisateur)
La requête retour l'utilisateur créé   : 

		{"id":0,"name":"toto","email":"toto@gmail.com"}   

	Si l'adresse mail est déjà utilisé un message d'erreur est renvoyé
    
		The user can't be created, the mail address may be already used

- **deleteUser?email=toto@gmail.com**
Suppression d'un utilisateur avec son email via 

		the user has been deleted

- **add?barCode=3124480208842&basket=1&userMail=toto@gmail.com**
Ajouter un produit à un panier
-> le produit possédant le code barre 3124480208842 et ainsi ajouté dans le panier 1 de l'utilisateur possédant l'adresse mail toto@gmail.com

	Remarque 1: Si le panier n'existe pas il est créé.
    
    Remarque 2: Si l'utilisateur ou le code barre n'existe pas une erreur est levée 

		the user or the code bar doesn't exist, can't add the product

- **delete?barCode=3029330003533&basket=1&userMail=toto@gmail.com**
Suppression d'un produit d'un panier
-> le produit possédant le code barre 3124480208842 et ainsi supprimé du panier 1 de l'utilisateur possédant l'adresse mail toto@gmail.com

	Remarque 1: Si le produit était le dernier du panier, celui-ci est supprimé
    
    Remarque 2: Si l'utilisateur ou le code barre n'existe pas ou bien que le produit n'est pas présent dans le panier une erreur est levée 
    
		the product can't be deleted

- **summary?basket=1&userMail=toto@gmail.com**
Obtenir un résumé sur l'ensemble des produits d'un panier ainsi que leurs valeurs nutritives.
Exemple de résultat :

		[  
 	  {  
      "id":1,
      "barCode":"3033490595517",
      "name":"Lait fermenté sucré aux fruits, au bifidus",
      "components":[  
         {  
            "id":6,
            "productId":1,
            "name":"energy_100g",
            "type":"N",
            "value":397.0,
            "quality":"Qualité",
            "points":1
         },{...}
      ],
      "nutritionScore":3,
      "quantity":1
	   },{...}
		]

- **product/?barCode=7622210449283**
Permet d'obtenir toutes les informations d'un produit en les récupérant que l'api open food facts
Par exemple le code barre 7622210449283 donnerait le résultat :

		{
		    "id": 0,
		    "barCode": "7622210449283",
		    "name": "Biscuits fourrés parfum chocolat (35%) - Prince Goût Chocolat au Blé Complet",
		    "components": [
		        {
		            "id": 0,
		            "productId": 0,
		            "name": "energy_100g",
		            "type": "N",
		            "value": 1955,
		            "quality": "Neutre",
		            "points": 5
		        },
		        {
		            "id": 0,
		            "productId": 0,
		            "name": "saturated-fat_100g",
		            "type": "N",
		            "value": 5.6,
		            "quality": "Neutre",
		            "points": 5
		        },
		        {
		            "id": 0,
		            "productId": 0,
		            "name": "sugars_100g",
		            "type": "N",
		            "value": 32,
		            "quality": "Défaut",
		            "points": 7
		        },
		        {
		            "id": 0,
		            "productId": 0,
		            "name": "salt_100g",
		            "type": "N",
		            "value": 0.58,
		            "quality": "Qualité",
		            "points": 0
		        },
		        {
		            "id": 0,
		            "productId": 0,
		            "name": "fiber_100g",
		            "type": "P",
		            "value": 4,
		            "quality": "Qualité",
		            "points": 4
		        },
		        {
		            "id": 0,
		            "productId": 0,
		            "name": "proteins_100g",
		            "type": "P",
		            "value": 6.4,
		            "quality": "Qualité",
		            "points": 4
		        }
		    ],
		    "nutritionScore": 9,
		    "quantity": 0
		}
		
    Remarque 1: Si le code barre n'existe pas une erreur est levée
     	The bar code doesn't exist, can't get the product
    
    Remarque 2: Si certaines inforamtions du produit ne sont pas trouvées sur l'api open food facts ceux ci sont mis à 0 pour les valeurs en composants et à "" pour le nom
 
- **nutritionscore/?score=20**
Permet d'obtenir le détail et les qualificatifs d'un score nutritionnel
Par exemple le score de 20 donnerait le résultat :

		{
		    "id": 5,
		    "color": "green",
		    "lowerBound": 19,
		    "classe": "Degueu",
		    "upperBound": 40
		}
