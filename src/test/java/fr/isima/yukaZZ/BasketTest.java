package fr.isima.yukaZZ;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.isima.yukaZZ.controller.BasketController;
import fr.isima.yukaZZ.dao.model.Product;
import fr.isima.yukaZZ.dto.ProductDto;
import fr.isima.yukaZZ.service.IBasketService;
import fr.isima.yukaZZ.service.IProductService;
import fr.isima.yukaZZ.service.IUserService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
@SpringBootTest
public class BasketTest {

	@Autowired
	IBasketService basketService;

	@Autowired
	IProductService productService;

	@Autowired
	IUserService userService;
	
	@Autowired
	BasketController basketController;

	/**
	 * addProductToBasket : product not existing in database
	 */
	@Test
	public void addProductToBasketWithProductNotExistingInDataBaseTest() {
		String mail = "basket3@gmail.com";

		List<ProductDto> products = basketService.summaryOfBasket(1, mail);
		assertTrue(products.isEmpty()); // the basket is empty at first

		//the product doesn't already exist in database
		String barCode = "3017760826174";
		int res = basketService.addProductToBasket(barCode, 1, mail);
		assertEquals(1, res);
		
		Product product = productService.getInfo(barCode);
		assertNotNull(product); // the product has been created
		assertEquals(barCode, product.getBarCode());

		products = basketService.summaryOfBasket(1, mail);
		assertEquals(barCode, products.get(0).getBarCode()); // the product has been had to the basket
	}
	
	/**
	 * addProductToBasket : product not existing in api
	 */
	@Test
	public void addProductToBasketWithProductNotExistingInApiTest() {
		String mail = "basket3@gmail.com";
		String barCode = "154545454";
		int res = basketService.addProductToBasket(barCode, 1, mail);
		assertEquals(-1, res);
	}

	/**
	 * addProductToBasket : basket not existing
	 */
	@Test
	public void addProductToBasketWithBasketNotExistingTest() {
		String mail = "basket3@gmail.com";

		Long basketId = basketService.getBasketId(2, mail);
		assertNull(basketId); // the basket doesn't exist at first

		basketId = null;
		String barCode = "3017760314190";
		int res = basketService.addProductToBasket(barCode, 2, mail);
		assertEquals(1, res);

		basketId = basketService.getBasketId(2, mail);
		assertNotNull(basketId); // the basket has been created

		List<ProductDto> products = basketService.summaryOfBasket(2, mail);
		assertEquals(barCode, products.get(0).getBarCode()); // the product has been had to the basket

	}

	/**
	 * addProductToBasket : everything already existing
	 */
	@Test
	public void addProductToBasketTest() {
		String mail = "basket3@gmail.com";
		String barCode = "3017760314190";

		Long userId = userService.getIdByEmail(mail);
		assertNotNull(userId); // the user exist

		Long basketId = basketService.getBasketId(4, mail);
		assertNotNull(basketId); // the basket exist

		Long productId = productService.getProductId(barCode);
		assertNotNull(productId); // the product exist

		List<ProductDto> products = basketService.summaryOfBasket(4, mail);
		assertTrue(products.isEmpty()); // the basket is empty at first

		int res = basketService.addProductToBasket(barCode, 4, mail);
		assertEquals(1, res);
	}

	/**
	 * addProductToBasket : user not existing
	 */
	@Test
	public void addProductToBasketWithUserNotExistingTest() {
		String mail = "userWhoDoesntExist@gmail.com";

		Long userId = userService.getIdByEmail(mail);
		assertNull(userId); // the user doesn't exist

		int res = basketService.addProductToBasket("ExistingBarCode", 3, mail);
		assertEquals(-1, res); // the product can't be add
	}

	/**
	 * getOrAddBasketId : case basket already exist
	 */
	@Test
	public void getOrAddBasketIdWithExixtingBasketTest() {
		String email = "basket@gmail.com";
		Long basketId = basketService.getOrAddBasketId(1, email);
		assertNotNull(basketId);
		assertTrue(basketId == 4);
	}

	/**
	 * getOrAddBasketId : case basket doesn't exist
	 */
	@Test
	public void getOrAddBasketIdWithNotExixtingBasketTest() {
		String email = "basket@gmail.com";
		Long basketId = null;
		basketId = basketService.getOrAddBasketId(2, email);
		assertNotNull(basketId);
	}

	/**
	 * getBasketId : case basket already exist
	 */
	@Test
	public void getBasketIdWithExistingBasketTest() {
		String email = "basket@gmail.com";
		Long basketId = basketService.getBasketId(1, email);
		assertNotNull(basketId);
		assertTrue(basketId == 4);
	}
	
	/**
	 * getBasketId : case basket not exist
	 */
	@Test
	public void getBasketIdWithNotExistingBasketTest() {
		String email = "basket@gmail.com";
		Long basketId = basketService.getBasketId(3, email);
		assertNull(basketId);
	}

	/**
	 * deleteProductOfBasket : case the product is the last one of the basket
	 */
	@Test
	public void deleteProductOfBasketTest() {
		String email = "basket2@gmail.com";
		int res = basketService.deleteProductOfBasket("7613034232465", 2, email);
		assertEquals(1, res);
		Long basketId = basketService.getBasketId(2, email);
		assertNull(basketId);			
	}
	
	/**
	 * deleteProductOfBasket : case the quantity of the product is more than 1
	 */
	@Test
	public void deleteProductOfBasketWithQuanityMoreThan1Test() {
		String email = "basket2@gmail.com";
		
		int res = basketService.deleteProductOfBasket("7613034232465", 3, email);
		assertEquals(1, res);
		List<ProductDto> products = basketService.summaryOfBasket(3, email);
		assertNotNull(products);
		assertEquals(1, products.size());
		assertEquals(1, products.get(0).getQuantity());
		assertEquals("7613034232465", products.get(0).getBarCode());
	}
	
	/**
	 * deleteProductOfBasket : case the quantity of the product is = 1 but its not the last product
	 */
	@Test
	public void deleteProductOfBasketWithQuantity1ButNotLastProductTest() {
		String email = "basket2@gmail.com";
		int res = basketService.deleteProductOfBasket("7613034232465", 4, email);
		assertEquals(1, res);
		List<ProductDto> products = basketService.summaryOfBasket(4, email);
		assertNotNull(products);
		assertEquals(1, products.size());
		assertEquals("7622210449283", products.get(0).getBarCode());
	}
	
	/**
	 * deleteProductOfBasket : case the product does not exist in the basket
	 */
	@Test
	public void deleteProductOfBasketWithProductDoesntExistTest() {
		String email = "basket2@gmail.com";
		int res = basketService.deleteProductOfBasket("productWhichDoesntExist", 4, email);
		assertEquals(-1, res);
	}

	/**
	 * Test the summary of a basket
	 */
	@Test
	public void summaryOfBasketTest() {
		List<ProductDto> products = basketService.summaryOfBasket(1, "basket2@gmail.com");
		assertNotNull(products);
		assertTrue(!products.isEmpty());
		assertEquals("7613034232465", products.get(0).getBarCode());
		assertEquals("7622210449283", products.get(1).getBarCode());
	}
	
	/**
	 * Test the summary of a basket when basket doesn't exist
	 */
	@Test
	public void summaryOfBasketWithBasketNotExistingTest() {
		ResponseEntity<?> res = basketController.summaryBasket(12, "basket2@gmail.com");
		assertEquals(HttpStatus.BAD_REQUEST, res.getStatusCode());
	}
	
	/**
	 * Test the summary of a basket when user doesn't exist
	 */
	@Test
	public void summaryOfBasketWithUserNotExistingTest() {
		ResponseEntity<?> res = basketController.summaryBasket(1, "false@email.com");
		assertEquals(HttpStatus.BAD_REQUEST, res.getStatusCode());
	}
}
