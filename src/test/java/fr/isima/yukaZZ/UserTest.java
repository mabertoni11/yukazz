package fr.isima.yukaZZ;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.isima.yukaZZ.controller.UserController;
import fr.isima.yukaZZ.dao.model.User;

@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration
@SpringBootTest
public class UserTest {
	
	@Autowired
	UserController userController;
		
	/**
	 * Test the creation of a user
	 */
	@Test
	public void CreateUserTest() {
		String email = "testUser@gmail.com";
		String name = "testUser";
		
		//verify the user doesn't already exist
		User user = userController.getUser(email);
		assertNull(user);
		
		ResponseEntity<?> response = userController.createUser(name,email );
		assertEquals(HttpStatus.OK, response.getStatusCode());
		
		//verify the user has been created
		user = userController.getUser(email);
		assertNotNull(user);
		assertEquals(name, user.getName());
		assertEquals(email, user.getEmail());		
	}
	
	/**
	 * Test the creation of a user
	 * case email already used
	 */
	@Test
	public void CreateUserWithAnEmailWichAlreadyExistTest() {
		String email = "userWhoAlreadyExist@gmail.com";
		String name2 = "testSecondUser";
		
		ResponseEntity<?> response = userController.createUser(name2,email);
		assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
	}
	
	/**
	 * Test the deletion of a user
	 * case user exist
	 */
	@Test
	public void deleteUserTest() {
		String email = "delete@gmail.com";
		
		//verify the user exist
		User user = userController.getUser(email);
		assertNotNull(user);
		
		ResponseEntity<String> res = userController.deleteUser(email);
		
		//verify the deletion of the user
		assertEquals(HttpStatus.OK, res.getStatusCode());
		user = userController.getUser(email);
		assertNull(user);
	}
	
	/**
	 * Test the deletion of a user
	 * case user not exist
	 */
	@Test
	public void deleteUserWithUserNotExistingTest() {
		ResponseEntity<String> res = userController.deleteUser("hkfvhjvhjv");
		assertEquals(HttpStatus.BAD_REQUEST, res.getStatusCode());
	}
}
