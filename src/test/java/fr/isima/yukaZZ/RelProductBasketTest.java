package fr.isima.yukaZZ;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.isima.yukaZZ.dao.model.RelProductBasket;
import fr.isima.yukaZZ.dto.ProductDto;
import fr.isima.yukaZZ.service.IRelProductBasketService;

@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration
@SpringBootTest
public class RelProductBasketTest {

	@Autowired
	IRelProductBasketService relService;
		
	/**
	 * Test increase the quantity of a relation
	 */
	@Test
	public void addQuantityTest() {
		long relId = 1;
		RelProductBasket rel = relService.getRelById(relId);
		assertEquals(1, rel.getQuantity());
		relService.addQuantity(relId);
		
		rel = relService.getRelById(relId);
		assertEquals(2, rel.getQuantity());
	}
	
	/**
	 * Test to get or create a relation
	 */
	@Test
	public void getOrCreateRelBasketProductTest() {
		//case relation already exist
		Long relId = relService.getOrCreateRelBasketProduct(0,0);
		assertNotNull(relId);
		assertTrue(relId == 0);
		
		//case relation doesn't exist
		relId = null;
		relId = relService.getOrCreateRelBasketProduct(0,1);
		assertNotNull(relId);
	}
	
	/**
	 * Test to get a relation
	 */
	@Test
	public void getRelBasketProductTest() {
		//case relation exist
		Long relId = relService.getRelBasketProduct(0,0);
		assertNotNull(relId);
		assertTrue(relId == 0);
		
		//case relation does not exist
		relId = relService.getRelBasketProduct(1,1);
		assertNull(relId);
	}
	
	/**
	 * Test the deletion of a product in a basket
	 * if quantity was 1 remove the relation
	 * otherwise -1
	 */
	@Test
	public void deleteProductInBasketTest() {
		RelProductBasket rel;
		
		//quantity = 1
		relService.deleteProductInBasket(3);
		rel = relService.getRelById(3);
		assertNull(rel);
		
		//quantity = 2
		relService.deleteProductInBasket(4);
		rel = relService.getRelById(4);
		assertNotNull(rel);
		assertEquals(4, rel.getId());
		assertEquals(1, rel.getQuantity());
	}
	
	/**
	 * Test the empty function
	 */
	@Test
	public void productOfBasketEmptyTest() {
		boolean res = relService.productOfBasketEmpty(4);
		assertTrue(res);
		
		res = relService.productOfBasketEmpty(0);
		assertFalse(res);
	}
	
	/**
	 * Test the function which recover all the product of a basket
	 */
	@Test
	public void getAllProductOfBasketTest(){
		List<ProductDto> products = relService.getAllProductOfBasket(1);
		
		assertEquals("3228850004031", products.get(0).getBarCode());
		assertEquals("7300400117869", products.get(1).getBarCode());
	};

}
