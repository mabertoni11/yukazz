package fr.isima.yukaZZ;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import fr.isima.yukaZZ.dao.model.NutritionScore;
import fr.isima.yukaZZ.service.INutritionScoreService;

/**
 * Class to test service for nutrition score
 *
 */
@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration
@SpringBootTest
public class NutritionScoreTest {

	/**
	 * The nutrition score service
	 */
	@Autowired
	INutritionScoreService nutritionScoreService;
	
	/**
	 * Test to get nutrition score of valid score
	 */
	@Test
	public void getValidNutritionScoresTest() {
		int id_Moins_10 = nutritionScoreService.getScore(-10).getId();
		assertTrue(id_Moins_10 == 1);
		int id_Moins_1 = nutritionScoreService.getScore(-1).getId();
		assertTrue(id_Moins_1 == 1);
		
		int id_0 = nutritionScoreService.getScore(0).getId();
		assertTrue(id_0 == 2);
		int id_2 = nutritionScoreService.getScore(2).getId();
		assertTrue(id_2 == 2);
		
		int id_3 = nutritionScoreService.getScore(3).getId();
		assertTrue(id_3 == 3);
		int id_10 = nutritionScoreService.getScore(10).getId();
		assertTrue(id_10 == 3);
		
		int id_11 = nutritionScoreService.getScore(11).getId();
		assertTrue(id_11 == 4);
		int id_18 = nutritionScoreService.getScore(18).getId();
		assertTrue(id_18 == 4);
		
		int id_19 = nutritionScoreService.getScore(19).getId();
		assertTrue(id_19 == 5);
		int id_40 = nutritionScoreService.getScore(40).getId();
		assertTrue(id_40 == 5);
	}
	
	/**
	 * Test to get nutrition score of invalid score
	 */
	@Test
	public void getInvalidNutritionScoresTest() {
		NutritionScore ns_Moins_20 = nutritionScoreService.getScore(-20);
		assertNull(ns_Moins_20);
		
		NutritionScore ns_45 = nutritionScoreService.getScore(45);
		assertNull(ns_45);
	}
}
