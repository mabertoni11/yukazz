package fr.isima.yukaZZ;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.isima.yukaZZ.dao.model.Component;
import fr.isima.yukaZZ.dao.model.Product;
import fr.isima.yukaZZ.dto.ProductDto;
import fr.isima.yukaZZ.service.IProductService;

@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration
@SpringBootTest
public class ProductTest {
	
	@Autowired
	IProductService productService;
		
	/**
	 * Test the function which get a product id by its bar code
	 * Create it if it doesn't exist
	 * case product already exist
	 */
	@Test
	public void getOrAddProductIdWithProductExistingTest() {
		Long prodId = productService.getOrAddProductId("7622210449283");
		assertNotNull(prodId);
		assertTrue(prodId == 6);
	
	}
	
	/**
	 * Test the function which get a product id by its bar code
	 * Create it if it doesn't exist
	 * case product doesn't exist
	 */
	@Test
	public void getOrAddProductIdWithProductNotExistingTest() {
		Long prodId = null;
		prodId = productService.getOrAddProductId("3033710065967");
		assertNotNull(prodId);
	}

	/**
	 * Test the function which get a product id by its bar code
	 * Do nothing if it doesn't exist
	 * case product exist
	 */
	@Test
	public void getProductIdProductExistingTest(){
		Long relId = productService.getProductId("7622210449283");
		assertNotNull(relId);
		assertTrue(relId == 6);
		
	}
	
	/**
	 * Test the function which get a product id by its bar code
	 * Do nothing if it doesn't exist
	 * case relation does not exist
	 */
	@Test
	public void getProductIdProductNotExistingTest(){
		Long relId = productService.getProductId("3073780258098");
		assertNull(relId);
	}
	
	/**
	 * Test the recovery of the information about a product
	 */
	@Test
	public void getInfoTest(){
		Product product = productService.getInfo("7622210449283");
		assertNotNull(product);
		
		assertEquals(6, product.getId());
		assertEquals("7622210449283", product.getBarCode());
	}
	
	/**
	 * Try to get all info of a product from its bar code
	 */
	@Test
	public void getInformationOfAProductTest(){
		ProductDto product = productService.getProductFromApi("7622210449283");
		assertNotNull(product);
		
		assertEquals("7622210449283",product.getBarCode());
		assertEquals("Biscuits fourrés parfum chocolat (35%) - Prince Goût Chocolat au Blé Complet",product.getName());
		assertEquals(9,product.getNutritionScore());
		
		List<Component> compos = product.getComponents();
		Component energyDensity = compos.get(0);
		Component saturatedFat = compos.get(1); 
		Component sugars = compos.get(2);
		Component salt = compos.get(3);
		Component fiber = compos.get(4);
		Component proteins = compos.get(5);
		
		assertEquals("energy_100g",energyDensity.getName());
		assertEquals(5, energyDensity.getPoints());
		assertEquals(product.getId(), energyDensity.getProductId());
		assertEquals("Neutre", energyDensity.getQuality());
		assertEquals('N', energyDensity.getType());
		assertTrue(1955.0 == energyDensity.getValue());

		assertEquals("saturated-fat_100g",saturatedFat.getName());
		assertEquals(5, saturatedFat.getPoints());
		assertEquals(product.getId(), saturatedFat.getProductId());
		assertEquals("Neutre", saturatedFat.getQuality());
		assertEquals('N', saturatedFat.getType());
		assertTrue(5.6 == saturatedFat.getValue());
		
		assertEquals("sugars_100g",sugars.getName());
		assertEquals(7, sugars.getPoints());
		assertEquals(product.getId(), sugars.getProductId());
		assertEquals("Défaut", sugars.getQuality());
		assertEquals('N', sugars.getType());
		assertTrue(32.0 == sugars.getValue());
		
		assertEquals("salt_100g",salt.getName());
		assertEquals(0, salt.getPoints());
		assertEquals(product.getId(), salt.getProductId());
		assertEquals("Qualité", salt.getQuality());
		assertEquals('N', salt.getType());
		assertTrue(0.58 == salt.getValue());
		
		assertEquals("fiber_100g",fiber.getName());
		assertEquals(4, fiber.getPoints());
		assertEquals(product.getId(), fiber.getProductId());
		assertEquals("Qualité", fiber.getQuality());
		assertEquals('P', fiber.getType());
		assertTrue(4.0 == fiber.getValue());
		
		assertEquals("proteins_100g",proteins.getName());
		assertEquals(4, proteins.getPoints());
		assertEquals(product.getId(), proteins.getProductId());
		assertEquals("Qualité", proteins.getQuality());
		assertEquals('P', proteins.getType());
		assertTrue(6.4 == proteins.getValue());
	}
	
	/**
	 * Try to get all info of a product from a not existing bar code
	 */
	@Test
	public void getInformationOfANotExistingProductTest(){
		ProductDto product = productService.getProductFromApi("q56sd489qs7d84s215qs89d9qs4d64");
		assertNull(product);
	}
}
