CREATE TABLE USER
(
    USER_ID  NUMERIC NOT NULL,
    USER_NAME VARCHAR2 NOT NULL,
    USER_EMAIL VARCHAR2 NOT NULL UNIQUE,
    PRIMARY KEY (USER_ID)
);

CREATE TABLE BASKET
(
   BASK_ID NUMERIC NOT NULL,
   USER_ID NUMERIC NOT NULL,
   BASK_NUMBER NUMERIC NOT NULL,
   PRIMARY KEY (BASK_ID),
   FOREIGN KEY (USER_ID) REFERENCES USER(USER_ID),
   CONSTRAINT UC_UserId_Number UNIQUE (USER_ID, BASK_NUMBER)
);

CREATE TABLE PRODUCT(
   PROD_ID NUMERIC NOT NULL,
   PROD_BAR_CODE VARCHAR NOT NULL UNIQUE,
   PROD_NAME VARCHAR2 NOT NULL,
   PROD_ENERGY_DENSITY FLOAT NOT NULL,
   PROD_SATURATED_FAT FLOAT NOT NULL,
   PROD_SUGARS FLOAT NOT NULL,
   PROD_SALT FLOAT NOT NULL,
   PROD_FIBER FLOAT NOT NULL,
   PROD_PROTEINS FLOAT NOT NULL,
   PROD_NUT_SCORE INT NOT NULL, 
   PRIMARY KEY (PROD_ID)
);

CREATE TABLE REL_PRODUCT_BASKET
(
	REL_ID NUMERIC NOT NULL,
  	REL_PROD_ID NUMERIC NOT NULL,
  	REL_BASK_ID NUMERIC NOT NULL,
  	REL_QUANTITY NUMERIC NOT NULL,
  	PRIMARY KEY (REL_ID),
   	FOREIGN KEY (REL_PROD_ID) REFERENCES PRODUCT(PROD_ID),
    FOREIGN KEY (REL_BASK_ID) REFERENCES BASKET(BASK_ID),
    CONSTRAINT UC_Prod_Bask UNIQUE (REL_PROD_ID,REL_BASK_ID)
);

CREATE TABLE COMPONENT(
    COMP_ID  NUMERIC NOT NULL,
    COMP_PRODID NUMERIC NOT NULL,
    COMP_NAME VARCHAR2 NOT NULL,
    COMP_TYPE VARCHAR2 NOT NULL,
    COMP_VALUE FLOAT NOT NULL,
    COMP_QUALITY VARCHAR2 NOT NULL,
    COMP_POINTS INT NOT NULL,
    PRIMARY KEY (COMP_ID),
    FOREIGN KEY (COMP_PRODID) REFERENCES PRODUCT(PROD_ID),
);

CREATE TABLE rule(
	id NUMERIC NOT NULL,
	name VARCHAR2 NOT NULL,
	points NUMERIC NOT NULL,
	min_bound NUMERIC NOT NULL,
	component VARCHAR2 NOT NULL,
	PRIMARY KEY (id)
);

CREATE TABLE nutrition_score(
	id NUMERIC NOT NULL,
	classe VARCHAR2 NOT NULL, 
	lower_bound NUMERIC NOT NULL,
	upper_bound NUMERIC NOT NULL,
	color VARCHAR2 NOT NULL,
	PRIMARY KEY (id)
); 