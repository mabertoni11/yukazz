package fr.isima.yukaZZ.service;

import fr.isima.yukaZZ.dao.model.User;

public interface IUserService {
	
	/**
	 * Get the user id by his email
	 * 
	 * @param email
	 * 			the user's email
	 * @return the user's id
	 */
	Long getIdByEmail(String email); 
	
	/**
	 * Get the user by his email
	 * 
	 * @param email
	 * 			the user's email
	 * @return the user
	 */
	User getUser(String email); 
	
	/**
	 * Create a user
	 * 
	 * @param name
	 * 			the user's name
	 * @param email
	 * 			the user's email
	 * @return the user created
	 */
	User create(String name, String email);

	/**
	 * Delete a user by his email
	 * 
	 * @param email
	 * 			the user's email
	 * @return 1 if the deletion is ok O otherwise
	 */
	int delete(String email);
}
