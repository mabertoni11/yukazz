package fr.isima.yukaZZ.service;

import java.util.List;

import fr.isima.yukaZZ.dto.ProductDto;

public interface IBasketService {
	
	/**
	 * Add a product to a basket, create a new relation if doesn't exist or increase the quantity
	 * 
	 * @param barCode
	 * 			the bar code of the product
	 * @param UserBasketId
	 * 			the basket id of the user in which add the product
	 * @param mail
	 * 			the email of the user
	 * @return if the addition is successful
	 */
	int addProductToBasket(String barCode, long UserBasketId, String mail);

	/**
	 * Get the the basket id of a user and his basket number, create it if it doesn't exist
	 * 
	 * @param UserBasketNumber
	 * 			the basket number of the user
	 * @param mail
	 * 			the user's mail
	 * @return the basket id
	 */
	long getOrAddBasketId(long UserBasketNumber, String mail);
	
	/**
	 * Get the the basket id of a user and his basket number
	 * 
	 * @param UserBasketNumber
	 * 			the basket number of the user
	 * @param mail
	 * 			the user's mail
	 * @return the basket id
	 */
	Long getBasketId(long UserBasketNumber, String mail);

	/**
	 * Delete a product of a basket, delete the basket if it was the last product
	 * 
	 * @param barCode
	 * 			the bar code of the product
	 * @param UserBasketNumber
	 * 			the basket number of the user
	 * @param mail
	 * 			the user's mail
	 * @return if the deletion has been done correctly
	 */
	int deleteProductOfBasket(String barCode, long UserBasketNumber, String mail);
	
	/**
	 * return all the information about all the product of a basket
	 * 
	 * @param UserBasketNumber
	 * 			the basket number of the user
	 * @param mail
	 * 			the user's mail
	 * @return the list of product
	 */
	List<ProductDto> summaryOfBasket(int UserBasketNumber, String mail);
}
