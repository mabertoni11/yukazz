package fr.isima.yukaZZ.service;

import java.util.List;

import fr.isima.yukaZZ.dao.model.RelProductBasket;
import fr.isima.yukaZZ.dto.ProductDto;

public interface IRelProductBasketService {
	
	/**
	 * Recover a relation between a a basket and a product, create it if it doesn't exist with quantity = 0
	 * 
	 * @param productId
	 * 			the product id
	 * @param basketId
	 * 			the basket id
	 * @return the id of the relation
	 */
	public long getOrCreateRelBasketProduct(long productId, long basketId);
	
	/**
	 * increase the quantity of a product in a basket
	 * 
	 * @param relId
	 * 			the relation id
	 */
	public void addQuantity(long relId);
	
	/**
	 * Get the relation between a product and a basket
	 * 
	 * @param productId
	 * 			the product id
	 * @param basketId
	 * 			the basket id
	 * @return the id of the relation
	 */
	public Long getRelBasketProduct(long productId, long basketId);
	
	/**
	 * Delete a product of a basket and delete the basket if it was the last product
	 * 
	 * @param relId
	 * 			the relation id
	 */
	public void deleteProductInBasket(long relId);
	
	/**
	 * Test if a basket is empty
	 * 
	 * @param basketId
	 * 			the basket id
	 * @return if the basket is empty
	 */
	public boolean productOfBasketEmpty(long basketId);
	
	/**
	 * Recover all the product of a basket
	 * 
	 * @param basketId
	 * 			the basket id
	 * @return a list of product
	 */
	public List<ProductDto> getAllProductOfBasket(long basketId);
	
	/**
	 * Recover the relation with its id
	 * 
	 * @param relId
	 * 			the relId
	 * @return the id
	 */
	public RelProductBasket getRelById(long relId);
}
