package fr.isima.yukaZZ.service;

import fr.isima.yukaZZ.dao.model.Product;
import fr.isima.yukaZZ.dto.ProductDto;

public interface IProductService {
	
	/**
	 * Get a product's id by his code bar or add it in the table if it doesn't exist
	 * 
	 * @param barCode
	 * 			the bar code of the product
	 * @return the id of the product
	 */
	long getOrAddProductId(String barCode);

	/**
	 * Get the id of an existing product
	 * 
	 * @param barCode
	 * 			the bar code of the product
	 * @return the id of the product
	 */
	Long getProductId(String barCode);
	
	/**
	 * Find a product by his id
	 * 
	 * @param id
	 * 			the id of the product
	 * @return the product found
	 */
	Product findById(long id);
	
	/**
	 * Get informations of a product
	 * 
	 * @param barCode
	 * 			the bar code of the product
	 * @return the product
	 */
	Product getInfo(String barCode);
	
	/**
	 * Get a product by its barCode
	 * @param barCode the Bar Code of the Product to get
	 * @return the Product corresponding to the bar code
	 */
	ProductDto getProductFromApi(String barCode);
}
