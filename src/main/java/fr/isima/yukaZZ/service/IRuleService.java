package fr.isima.yukaZZ.service;

/**
 * The Service for Rule Object
 *
 */
public interface IRuleService {
	/**
	 * Get the points of a component in a product
	 * @param criteria the component name
	 * @param value the content of component in the product
	 * @param type the 'N' or 'P' type of the component
	 * @return the points corresponding to the component
	 */
	int getPointsOfComponent(String criteria, double value,char type);
}
