package fr.isima.yukaZZ.service;

import java.util.List;

import fr.isima.yukaZZ.dao.model.Component;

public interface IComponentService {
	
	public void saveAllComponent(List<Component> components, long prodId);
	
	public List<Component> getAllComponentOfProduct(long prodId);

}
