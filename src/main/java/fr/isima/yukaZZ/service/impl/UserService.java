package fr.isima.yukaZZ.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.isima.yukaZZ.dao.model.User;
import fr.isima.yukaZZ.repository.IUserRepository;
import fr.isima.yukaZZ.service.IUserService;

@Service
public class UserService implements IUserService {

	/**
	 * the user repository
	 */
	@Autowired
	IUserRepository userRepo;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Long getIdByEmail(String email) {
		User user = userRepo.findByEmail(email);
		Long result = null;
		if (user != null)
			result = user.getId();
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public User create(String name, String email) {
		User result = null;
		long count = userRepo.count();
		User user = userRepo.findByEmail(email);
		if (user == null) //the user doesn't already exist
			result = userRepo.save(new User(count, name, email));
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int delete(String email) {
		int res = 1;
		User user = userRepo.findByEmail(email);
		if (user == null)
			res = -1;
		else
			userRepo.deleteById(user.getId());
		return res;
	}

	/**
	 * {@inheritDoc}
	 */
	public User getUser(String email) {
		return userRepo.findByEmail(email);
	}
}
