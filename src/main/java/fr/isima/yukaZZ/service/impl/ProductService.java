package fr.isima.yukaZZ.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.isima.yukaZZ.dao.model.Component;
import fr.isima.yukaZZ.dao.model.Product;
import fr.isima.yukaZZ.dto.ProductDto;
import fr.isima.yukaZZ.dto.ProductFactory;
import fr.isima.yukaZZ.repository.IOpenFoodAPIRepository;
import fr.isima.yukaZZ.repository.IProductRepository;
import fr.isima.yukaZZ.repository.IRuleRepository;
import fr.isima.yukaZZ.service.IComponentService;
import fr.isima.yukaZZ.service.IProductService;

/**
 * The implementation of the Product Service
 *
 */
@Service
public class ProductService implements IProductService {

	/**
	 * the product repository
	 */
	@Autowired
	IProductRepository productRepo;

	/**
	 * The open food repository
	 */
	@Autowired
	IOpenFoodAPIRepository openFoodAPIRepo;

	/**
	 * The rule repository
	 */
	@Autowired
	IRuleRepository ruleRepo;

	/**
	 * The component service
	 */
	@Autowired
	IComponentService componentService;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ProductDto getProductFromApi(String barCode) {
		Product entity = openFoodAPIRepo.getProduct(barCode);
		if(entity == null) {
			return null;
		}
		ProductDto dto = ProductFactory.ProductEntityToDto(entity);

		int index = 0;
		int sommeN = 0;
		int sommeP = 0;
		for (Component compo : dto.getComponents()) {
			int points = ruleRepo.getRule(compo.getName(), compo.getValue(), compo.getType());
			compo.setPoints(points);
			if (compo.getType() == 'P') {
				compo.setQuality(points >= 2 ? "Qualité" : (points <= 0 ? "Défaut" : "Neutre"));
				sommeP += points;
			} else {
				compo.setQuality(points <= 3 ? "Qualité" : (points >= 7 ? "Défaut" : "Neutre"));
				sommeN += points;
			}
			dto.ChangeComponent(index, compo);
			dto.setNutritionScore(sommeN - sommeP);
			index++;
		}

		return dto;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public long getOrAddProductId(String barCode) {
		Product product = productRepo.findByBarCode(barCode);
		long result = -1;
		// the product doesn't exist create it
		if (product == null) {
			long count = productRepo.count();
			// get information for the api
			product = openFoodAPIRepo.getProduct(barCode);
			if (product != null) {
				product.setId(count);

				// get analyze and save components
				ProductDto productDto = getProductFromApi(barCode);
				product.setNutritionScore(productDto.getNutritionScore());
				productRepo.save(product);
				componentService.saveAllComponent(productDto.getComponents(), product.getId());
				result = product.getId();
			} 
		}else {
			result = product.getId();
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Long getProductId(String barCode) {
		Product product = productRepo.findByBarCode(barCode);
		Long id = null;
		if (product != null)
			id = product.getId();
		return id;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Product findById(long id) {
		return productRepo.findById(id);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Product getInfo(String barCode) {
		return productRepo.findByBarCode(barCode);
	}
}
