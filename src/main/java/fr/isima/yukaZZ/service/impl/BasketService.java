package fr.isima.yukaZZ.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.isima.yukaZZ.dao.model.Basket;
import fr.isima.yukaZZ.dto.ProductDto;
import fr.isima.yukaZZ.repository.IBasketRepository;
import fr.isima.yukaZZ.service.IBasketService;
import fr.isima.yukaZZ.service.IProductService;
import fr.isima.yukaZZ.service.IRelProductBasketService;
import fr.isima.yukaZZ.service.IUserService;

@Service
public class BasketService implements IBasketService {

	/**
	 * basket repository
	 */
	@Autowired
	IBasketRepository basketRepo;

	/**
	 * product service
	 */
	@Autowired
	IProductService productService;

	/**
	 * user service
	 */
	@Autowired
	IUserService userService;

	/**
	 * relProductBasket service
	 */
	@Autowired
	IRelProductBasketService relProductBasketService;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int addProductToBasket(String barCode, long UserBasketNumber, String mail) {
		long productId = productService.getOrAddProductId(barCode);
		long basketId = this.getOrAddBasketId(UserBasketNumber, mail);

		if (basketId != -1 && productId != -1) {
			long relId = relProductBasketService.getOrCreateRelBasketProduct(productId, basketId);
			relProductBasketService.addQuantity(relId);
			return 1;
		} else { // the user with mail doesn't exist or the code bar doesn't exist
			return -1;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public long getOrAddBasketId(long UserBasketNumber, String mail) {
		long result = -1;
		Long userId = userService.getIdByEmail(mail);

		if (userId != null) { // the user exist
			Basket basket = basketRepo.findByUserIdAndUserBasketNumber(userId, UserBasketNumber);

			// the product doesn't exist create it
			if (basket == null) {
				long count = basketRepo.count();
				basket = new Basket(count, userId, UserBasketNumber);
				basketRepo.save(basket);
			}
			result = basket.getId();
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Long getBasketId(long UserBasketNumber, String mail) {
		Long userId = userService.getIdByEmail(mail);
		Long id = null;
		if (userId != null) {
			Basket basket = basketRepo.findByUserIdAndUserBasketNumber(userId, UserBasketNumber);
			if (basket != null)
				id = basket.getId();
		}
		return id;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int deleteProductOfBasket(String barCode, long UserBasketNumber, String mail) {
		int err = 1;
		Long productId = productService.getProductId(barCode);
		Long basketId = this.getBasketId(UserBasketNumber, mail);

		// throw an error if the product or the basket doesn't exist
		if (productId == null || basketId == null)
			err = -1;
		else {
			Long relId = relProductBasketService.getRelBasketProduct(productId, basketId);
			if (relId == null)
				err = -1;
			else {
				relProductBasketService.deleteProductInBasket(relId);
				if (relProductBasketService.productOfBasketEmpty(basketId)) {
					basketRepo.deleteById(basketId);
				}
			}
		}
		return err;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ProductDto> summaryOfBasket(int UserBasketNumber, String mail) {
		Long basketId = this.getBasketId(UserBasketNumber, mail);
		List<ProductDto> products = null;
		if (basketId != null) {
			products = relProductBasketService.getAllProductOfBasket(basketId);
		}
		return products;
	}
}
