package fr.isima.yukaZZ.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.isima.yukaZZ.dao.model.Component;
import fr.isima.yukaZZ.repository.IComponentRepository;
import fr.isima.yukaZZ.service.IComponentService;

@Service
public class ComponentService implements IComponentService{
	
	/**
	 * basket repository
	 */
	@Autowired
	IComponentRepository componentRepo;
	
	@Override
	public void saveAllComponent(List<Component> components, long prodId) {
		long count = componentRepo.count();
		for (Component c : components) {
			c.setProductId(prodId);
			c.setId(count);
			componentRepo.save(c);
			count++;
		}
	}
	
	@Override
	public List<Component> getAllComponentOfProduct(long prodId){
		return componentRepo.findByProductId(prodId);
	}

}
