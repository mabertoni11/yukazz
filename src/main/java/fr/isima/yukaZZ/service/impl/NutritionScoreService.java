package fr.isima.yukaZZ.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.isima.yukaZZ.dao.model.NutritionScore;
import fr.isima.yukaZZ.repository.INutritionScoreRepository;
import fr.isima.yukaZZ.service.INutritionScoreService;

/**
 * The implementation of INutritionScoreService
 *
 */
@Service
public class NutritionScoreService implements INutritionScoreService{

	/**
	 * The Nutrition Score Repository
	 */
	@Autowired
	INutritionScoreRepository nutritionScoreRepo;
	
	@Override
	public NutritionScore getScore(int score) {
		return nutritionScoreRepo.getNutritionScore(score);
	}

}
