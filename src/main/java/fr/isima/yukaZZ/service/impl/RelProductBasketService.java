package fr.isima.yukaZZ.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.isima.yukaZZ.dao.model.Component;
import fr.isima.yukaZZ.dao.model.Product;
import fr.isima.yukaZZ.dao.model.RelProductBasket;
import fr.isima.yukaZZ.dto.ProductDto;
import fr.isima.yukaZZ.repository.IRelProductBasketRepository;
import fr.isima.yukaZZ.service.IComponentService;
import fr.isima.yukaZZ.service.IProductService;
import fr.isima.yukaZZ.service.IRelProductBasketService;

@Service
public class RelProductBasketService implements IRelProductBasketService{
	
	/**
	 * the repository for the relation between basket and product
	 */
	@Autowired
	IRelProductBasketRepository relProductBasketRepo;
	
	/**
	 * the product service
	 */
	@Autowired
	IProductService productService;
	
	/**
	 * the component service
	 */
	@Autowired
	IComponentService componentService;
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public long getOrCreateRelBasketProduct(long productId, long basketId) {
		RelProductBasket rel = relProductBasketRepo.findByProductIdAndBasketId(productId, basketId);
		
		if (rel == null) {
			long count = relProductBasketRepo.count();
			rel = new RelProductBasket(count, productId, basketId);
			relProductBasketRepo.save(rel);
		}
		return rel.getId();
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Long getRelBasketProduct(long productId, long basketId) {
		RelProductBasket rel = relProductBasketRepo.findByProductIdAndBasketId(productId, basketId);
		Long id = null;
		if (rel != null) id = rel.getId();
		return id;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void addQuantity(long relId) {
		RelProductBasket rel = relProductBasketRepo.findById(relId);
		rel.addQuantity();
		
		relProductBasketRepo.save(rel);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteProductInBasket(long relId) {
		RelProductBasket rel = relProductBasketRepo.findById(relId);
		if (rel.getQuantity() > 1) {
			rel.removeQuantity();
			relProductBasketRepo.save(rel);
		} else {
			relProductBasketRepo.deleteById(relId);
		}
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean productOfBasketEmpty(long basketId) {
		List<RelProductBasket> products = relProductBasketRepo.findByBasketId(basketId);
		boolean empty = false;
		if (products.isEmpty()) {
			empty = true;
		}
		return empty;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ProductDto> getAllProductOfBasket(long basketId){
		List<RelProductBasket> relProdBask = relProductBasketRepo.findByBasketId(basketId);
		
		List<ProductDto> productsDto = new ArrayList();
		for (RelProductBasket rel : relProdBask) {
			Product product = productService.findById(rel.getProductId());
			List<Component> components = componentService.getAllComponentOfProduct(rel.getProductId());
			ProductDto p = new ProductDto(product.getId(), product.getBarCode(), product.getName(), 
					components, product.getNutritionScore(), rel.getQuantity());
			productsDto.add(p);
		}
		return productsDto;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public RelProductBasket getRelById(long relId) {
		return relProductBasketRepo.findById(relId);
	}
	
}
