package fr.isima.yukaZZ.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.isima.yukaZZ.repository.IRuleRepository;
import fr.isima.yukaZZ.service.IRuleService;

/**
 * The implementation of the Rule Service
 *
 */
@Service
public class RuleService implements IRuleService {
	
	/**
	 * The rule repository
	 */
	@Autowired
	IRuleRepository ruleRepo;
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getPointsOfComponent(String criteria, double value,char type) {
		return ruleRepo.getRule(criteria, value, type);
	}

}
