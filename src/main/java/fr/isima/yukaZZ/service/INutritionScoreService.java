package fr.isima.yukaZZ.service;

import fr.isima.yukaZZ.dao.model.NutritionScore;

/**
 * The Service for NutritionScore Object
 *
 */
public interface INutritionScoreService {
	/**
	 * Get the nutrition score form a product score
	 * @param score the product score
	 * @return the nutrition score corresponding to the score
	 */
	NutritionScore getScore(int score);
}