package fr.isima.yukaZZ.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fr.isima.yukaZZ.dto.ProductDto;
import fr.isima.yukaZZ.service.IBasketService;

@RestController
public class BasketController {

	/**
	 * service used to communicate with the basket table
	 */
	@Autowired
	IBasketService service;

	/**
	 * Add a product to a product of a user, create the basket if it doesn't exist
	 * 
	 * @param barCode
	 *            the bar code of the product to add
	 * @param basket
	 *            the basket in which the product has to be added
	 * @param mail
	 *            the mail of the user
	 * @return the status of the action
	 */
	@RequestMapping("/add")
	public ResponseEntity<String> addProduct(
			@RequestParam(value = "barCode", defaultValue = "0000000000000") String barCode,
			@RequestParam(value = "basket", defaultValue = "0") int basket,
			@RequestParam(value = "userMail") String mail) {
		System.out.println("add product " + barCode + " to basket " + basket);
		int res = this.service.addProductToBasket(barCode, basket, mail);
		if (res == 1)
			return ResponseEntity.ok("the product has been added");
		else
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.body("the user or the code bar doesn't exist, can't add the product");
	}

	/**
	 * Delete a product of a basket (only one instance), delete the basket id it's
	 * empty after the deletion
	 * 
	 * @param barCode
	 *            the bar code of the product to delete
	 * @param basket
	 *            the basket in which the product is
	 * @param mail
	 *            the mail of the user
	 * @return the status of the request
	 */
	@RequestMapping("/delete")
	public ResponseEntity<?> deleteProduct(
			@RequestParam(value = "barCode", defaultValue = "0000000000000") String barCode,
			@RequestParam(value = "basket", defaultValue = "0") int basket,
			@RequestParam(value = "userMail") String mail) {
		System.out.println("delete product " + barCode + " to basket " + basket);
		int res = this.service.deleteProductOfBasket(barCode, basket, mail);

		HttpStatus status = HttpStatus.BAD_REQUEST;
		if (res != 1)
			return ResponseEntity.status(status).body("the product can't be deleted");
		else
			return ResponseEntity.ok("the product has been deleted");
	}

	/**
	 * Realize a summary of the basket of a user
	 * 
	 * @param basket
	 *            the basket number
	 * @param mail
	 *            the mail of the user
	 * @return the status of the request
	 */
	@RequestMapping("/summary")
	public ResponseEntity<?> summaryBasket(@RequestParam(value = "basket", defaultValue = "0") int basket,
			@RequestParam(value = "userMail") String mail) {
		System.out.println("summary basket " + basket);

		List<ProductDto> products = this.service.summaryOfBasket(basket, mail);
		if (products == null)
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("The user or the basket doesn't exist");
		else
			return ResponseEntity.status(HttpStatus.OK).body(products);

	}
}
