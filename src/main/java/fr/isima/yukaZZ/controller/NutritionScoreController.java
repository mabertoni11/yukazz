package fr.isima.yukaZZ.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import fr.isima.yukaZZ.dao.model.NutritionScore;
import fr.isima.yukaZZ.service.INutritionScoreService;


/**
 * Controller for the NutritionScore object
 *
 */
@RestController
public class NutritionScoreController {
	/**
	 * The Nutrition Score service
	 */
	@Autowired
	INutritionScoreService nutritionScoreService;
	
	/**
	 * Get a product
	 * 
	 * @param score the bar code of the product
	 * @return the product corresponding to the bar code 
	 */
	@RequestMapping("/nutritionscore/")
	public ResponseEntity<NutritionScore> getInformationOfAProduct(@RequestParam(value="score") int score){
		return ResponseEntity.ok(this.nutritionScoreService.getScore(score));
	}

}
