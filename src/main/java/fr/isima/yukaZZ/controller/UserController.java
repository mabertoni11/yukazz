package fr.isima.yukaZZ.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fr.isima.yukaZZ.dao.model.User;
import fr.isima.yukaZZ.service.IUserService;

@RestController
public class UserController {

	/**
	 * service used to communicate with the user table
	 */
	@Autowired
	IUserService service;

	/**
	 * Create a user
	 * 
	 * @param name
	 *            the name of the user to save
	 * @param email
	 *            the email of the user to save
	 * @return the user saved
	 */
	@RequestMapping("/addUser")
	public ResponseEntity<?> createUser(@RequestParam(value = "name", defaultValue = "toto") String name,
			@RequestParam(value = "email", defaultValue = "0") String email) {
		System.out.println("add user " + name + " ; " + email);
		User user = this.service.create(name, email);
		if (user != null)
			return ResponseEntity.ok(user);
		else
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.body("The user can't be created, the mail address may be already used");
	}

	/**
	 * Delete a user
	 * 
	 * @param email
	 *            the email of the user to delete
	 * @return the httpStatus
	 */
	@RequestMapping("/deleteUser")
	public ResponseEntity<String> deleteUser(@RequestParam(value = "email", defaultValue = "0") String email) {
		System.out.println("delete user " + email);
		int res = this.service.delete(email);
		if (res == 1)
			return ResponseEntity.ok("the user has been deleted");
		else
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("the user can't be deleted");
	}

	public User getUser(String email) {
		return this.service.getUser(email);
	}
}
