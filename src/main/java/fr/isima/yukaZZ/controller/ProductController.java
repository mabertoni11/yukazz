package fr.isima.yukaZZ.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import fr.isima.yukaZZ.dto.ProductDto;
import fr.isima.yukaZZ.service.IProductService;

/**
 * Controller for the Product object
 *
 */
@RestController
public class ProductController {
	/**
	 * The Product service
	 */
	@Autowired
	IProductService service;
	
	/**
	 * Get a product
	 * 
	 * @param barCode the bar code of the product
	 * @return the product corresponding to the bar code 
	 */
	@RequestMapping("/product/")
	public ResponseEntity<?> getInformationOfAProduct(@RequestParam(value="barCode") String barCode){
		ProductDto result = this.service.getProductFromApi(barCode);
		if(result == null) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.body("The bar code doesn't exist, can't get the product");
		}
		return ResponseEntity.ok(result);
	}

}
