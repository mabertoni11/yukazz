package fr.isima.yukaZZ;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Component;

/**
 * Class to start the App
 *
 */
@SpringBootApplication
@Component
public class YukaZZ 
{
	/**
	 * Main method running the app
	 * 
	 * @param args args of the application
	 */
	@SuppressWarnings("unused")
    public static void main( String[] args )
    {
		SpringApplication.run(YukaZZ.class, args);
    }
    
}
