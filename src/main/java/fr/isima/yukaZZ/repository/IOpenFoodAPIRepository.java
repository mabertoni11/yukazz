package fr.isima.yukaZZ.repository;

import fr.isima.yukaZZ.dao.model.Product;

/**
 * Repository to manage data from the API OpenFoodFact
 *
 */
public interface IOpenFoodAPIRepository {
	
	/**
	 * Get a product from the API
	 * @param barCode the bar code of the product to search
	 * @return the expected product
	 */
	Product getProduct(String barCode);
}
