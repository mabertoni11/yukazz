package fr.isima.yukaZZ.repository;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Repository;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

import fr.isima.yukaZZ.dao.model.Product;

/**
 * The Implementation of the Repository IOpenFoodAPIRepository to manage Data from Open Food Facts
 *
 */
@Repository
public class IOpenFoodAPIRepositoryImpl implements IOpenFoodAPIRepository{

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Product getProduct(String barCode) {
		HttpResponse<JsonNode> request;
		
		try {
			request = Unirest.get("https://fr.openfoodfacts.org/api/v0/produit/"+barCode+".json").asJson();
			
			JSONObject product;
			
			// Try to get "product" field in JSON, if not found return null Product
			try {
				product = request.getBody().getObject().getJSONObject("product");
			}catch(JSONException e) {
				return null;
			}
			
			String name;
			double energyDensity;
			double saturatedFat;
			double sugars;
			double salt;
			double fiber;
			double proteins;
			
			// Try to get all components field in the JSON, if not found they are set to 0
			try {
				name = product.getString("generic_name_fr");
			} catch (JSONException  e) {
				name = "";
			}
			
			try {
				energyDensity = product.getJSONObject("nutriments").getDouble("energy_100g");
			} catch (JSONException  e) {
				energyDensity = 0;
			}
			
			try {
				saturatedFat = product.getJSONObject("nutriments").getDouble("saturated-fat_100g");
			} catch (JSONException  e) {
				saturatedFat = 0;
			}

			try {
				sugars = product.getJSONObject("nutriments").getDouble("sugars_100g");
			} catch (JSONException  e) {
				sugars = 0;
			}
			
			try {
				salt = product.getJSONObject("nutriments").getDouble("salt_100g");
			} catch (JSONException  e) {
				salt = 0;
			}
			
			try {
				fiber = product.getJSONObject("nutriments").getDouble("fiber_100g");
			} catch (JSONException  e) {
				fiber = 0;
			}
			
			try {
				proteins = product.getJSONObject("nutriments").getDouble("proteins_100g");
			} catch (JSONException  e) {
				proteins = 0;
			}
					  
			return new Product(barCode, name, energyDensity, saturatedFat, sugars, salt, fiber, proteins);
		} catch (UnirestException e1) {
			e1.printStackTrace();
		}
		return null;
	}
}