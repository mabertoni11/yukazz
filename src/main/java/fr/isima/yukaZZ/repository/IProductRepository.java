package fr.isima.yukaZZ.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.isima.yukaZZ.dao.model.Product;

/**
 * Repository connected with the Product table
 */
@Repository
public interface IProductRepository extends JpaRepository<Product, Long>{
	
	/**
	 * Found a product by his bar code
	 * 
	 * @param barCode
	 * 			the bar code of the product to find
	 * @return the product found
	 */
	Product findByBarCode(String barCode);
	
	/**
	 * Found a product by his id
	 * 
	 * @param id
	 * 			the id of the product
	 * @return the product found
	 */
	Product findById(long id);
}
