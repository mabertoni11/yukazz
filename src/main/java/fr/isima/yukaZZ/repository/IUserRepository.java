package fr.isima.yukaZZ.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.isima.yukaZZ.dao.model.User;

/**
 * Repository connected with the user table
 */
@Repository
public interface IUserRepository extends JpaRepository<User, Long>{
	
	/**
	 * Find a user by his name
	 * 
	 * @param name
	 * 			the user's name
	 * @return the user
	 */
	User findByName(String name);
	
	/**
	 * Find a user by his email
	 * 
	 * @param email
	 * 			the user's email
	 * @return the user
	 */
	User findByEmail(String email);

}
