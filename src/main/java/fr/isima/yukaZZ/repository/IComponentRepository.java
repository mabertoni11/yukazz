package fr.isima.yukaZZ.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.isima.yukaZZ.dao.model.Component;

@Repository
public interface IComponentRepository extends JpaRepository<Component, Long>{
	
	/**
	 * Find all the components of a product
	 * @param productId
	 * 			the product id
	 * @return the list of component
	 */
	List<Component> findByProductId(long productId);

}
