package fr.isima.yukaZZ.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import fr.isima.yukaZZ.dao.model.RelProductBasket;

/**
 * Repository connected with the table which link basket and product
 */
@Repository
public interface IRelProductBasketRepository  extends JpaRepository<RelProductBasket, Long> {
	
	/**
	 * Find the relation between a product and a basket by their id
	 * 
	 * @param productId
	 * 			the product id
	 * @param basketId
	 * 			the basket id
	 * @return the relation between the product and the basket if exist
	 */
	RelProductBasket findByProductIdAndBasketId(long productId, long basketId);
	
	/**
	 * Find the relation between a product and a basket by the id 
	 * 
	 * @param id
	 * 			the id of the relation
	 * @return the relation if exist
	 */
	RelProductBasket findById(long id);
	
	/**
	 * Delete a relation between a product and a basket
	 * 
	 * @param id
	 * 		the id of the relation
	 */
	void deleteById(long id);
	
	/**
	 * Find all the product on a basket
	 * 
	 * @param basketId
	 * 			the basket id
	 * @return the list of the relation basket/product
	 */
	List<RelProductBasket> findByBasketId(long basketId);

}
