package fr.isima.yukaZZ.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import fr.isima.yukaZZ.dao.model.NutritionScore;

/**
 * The Repository to manage the NutritionScore object
 *
 */
@Repository
public interface INutritionScoreRepository extends JpaRepository<NutritionScore, Long>{
	
	/**
	 * Get a nutrition score corresponding to the score of a product
	 * @param score the score of the product
	 * @return the nutrition Score corresponding to the input score
	 */
	@Query("SELECT ns FROM NutritionScore ns WHERE ns.LowerBound <= :score AND ns.UpperBound >= :score")
	NutritionScore getNutritionScore(@Param("score") int score);

}
