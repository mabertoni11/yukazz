package fr.isima.yukaZZ.repository;

import org.springframework.stereotype.Repository;

import fr.isima.yukaZZ.dao.model.Rule;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * The repository to manage Rule data
 *
 */
@Repository
public interface IRuleRepository extends JpaRepository<Rule, Long>{
	
	/**
	 * Get the points (interval) corresponding to a component
	 * @param component the component name
	 * @param value the content of component in the product
	 * @param type the 'N' or 'P' type of the component
	 * @return the points corresponding to the component in the product
	 */
	@Query(value ="SELECT max(r.points) FROM rule r WHERE r.name = :component AND r.component = :type AND r.min_bound<= :value", nativeQuery = true)
	int getRule(@Param("component") String component, @Param("value") double value, @Param("type") char type);
	
}
