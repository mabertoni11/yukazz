package fr.isima.yukaZZ.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.isima.yukaZZ.dao.model.Basket;

/**
 * Repository connected with the Basket table
 */
@Repository
public interface IBasketRepository extends JpaRepository<Basket, Long>{
	
	/**
	 * Find the basket of a user
	 * 
	 * @param userId
	 * 			the id of the owner of the basket
	 * @param userBasketNumber
	 * 			the number of the basket own by the user
	 * @return the basket found
	 */
	Basket findByUserIdAndUserBasketNumber(long userId, long userBasketNumber);
	
	/**
	 * Find a basket with his id
	 * 
	 * @param basketId
	 * 			the id of the basket
	 * @return the basket found
	 */
	Basket findById(long basketId);
	
	/**
	 * Delete a basket with his id
	 * 
	 * @param id
	 * 		the id of the basket
	 */
	void deleteById(long id);

}