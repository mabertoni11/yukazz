package fr.isima.yukaZZ.dao.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Class to recover information of users of the database
 */

@Entity
@Table(name = "USER")
public class User {
	
	@Id
	@Column(name = "USER_ID", nullable = false)
	private long id;
	
	@Column(name = "USER_NAME", nullable = false)
	private String name;
	
	@Column(name = "USER_EMAIL", nullable = false)
	private String email;
	
	public User(final long pId, final String pName, final String pEmail) {
		this.id = pId;
		this.name = pName;
		this.email = pEmail;
	}
	
	public User() {
		
	}

	public String getName() {
		return name;
	}
	
	public long getId() {
		return id;
	}
	
	public String getEmail() {
		return email;
	}
}
