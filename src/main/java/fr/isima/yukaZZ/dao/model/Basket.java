package fr.isima.yukaZZ.dao.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Class to recover information of baskets of the database
 */

@Entity
@Table(name = "BASKET")
public class Basket {
	
	@Id
	@Column(name = "BASK_ID", nullable = false)
	private long id;
	
	@Column(name = "USER_ID", nullable = false)
	private long userId;
	
	@Column(name = "BASK_NUMBER", nullable = false)
	private long userBasketNumber;
	
	public Basket(final long id, final long userId, final long userBasketNumber2) {
		this.id = id;
		this.userId = userId;
		this.userBasketNumber = userBasketNumber2;
	}

	public long getId() {
		return id;
	}
	
	public Basket() {}

}
	