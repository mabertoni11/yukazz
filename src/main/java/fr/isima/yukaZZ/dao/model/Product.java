package fr.isima.yukaZZ.dao.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * The Model of a Product object
 *
 */
@Entity
@Table(name = "PRODUCT")
public class Product {

	/**
	 * The Id of the Product
	 */
	@Id
	@Column(name = "PROD_ID", nullable = false)
	private long id;
	
	/**
	 * The Bar Code of the product
	 */
	@Column(name = "PROD_BAR_CODE", nullable = false)
	private String barCode;
	
	/**
	 * The Name of the product
	 */
	@Column(name = "PROD_NAME", nullable = false)
	private String name;
	
	/**
	 * The Energy density of the product
	 */
	@Column(name = "PROD_ENERGY_DENSITY", nullable = false)
	private double energyDensity;
	
	/**
	 * The Saturated Fat of the product
	 */
	@Column(name = "PROD_SATURATED_FAT", nullable = false)
	private double saturatedFat;
	
	/**
	 * The Sugars of the product
	 */
	@Column(name = "PROD_SUGARS", nullable = false)
	private double sugars;
	
	/**
	 * The Salt of the product
	 */
	@Column(name = "PROD_SALT", nullable = false)
	private double salt;
	
	/**
	 * The Fiber of the product
	 */
	@Column(name = "PROD_FIBER", nullable = false)
	private double fiber;
	
	/**
	 * The Proteins of the product
	 */
	@Column(name = "PROD_PROTEINS", nullable = false)
	private double proteins;
	
	/**
	 * The Nutrition Score of the product 
	 */
	@Column(name = "PROD_NUT_SCORE", nullable = false)
	private int nutritionScore;
	
	
	/**
	 * The public default constructor of the product
	 */
	public Product() {
	}
	
	/**
	 * The public constructor of the product
	 * @param barCode the product's bar code
	 * @param name the product's name
	 * @param energyDensity the product's energy density
	 * @param saturatedFat the product's saturated fat
	 * @param sugars the product's sugars
	 * @param salt the product's salts
	 * @param fiber the product's fiber
	 * @param proteins the product's proteins
	 */
	public Product(String barCode, String name, double energyDensity, double saturatedFat, double sugars, 
			double salt, double fiber, double proteins) {
		this.barCode = barCode;
		this.name = name;
		
		this.energyDensity = energyDensity;
		this.saturatedFat = saturatedFat;
		this.sugars = sugars;
		this.salt = salt;
		this.fiber = fiber;
		this.proteins = proteins;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getBarCode() {
		return barCode;
	}

	public void setBarCode(String barCode) {
		this.barCode = barCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getEnergyDensity() {
		return energyDensity;
	}

	public void setEnergyDensity(double energyDensity) {
		this.energyDensity = energyDensity;
	}

	public double getSaturatedFat() {
		return saturatedFat;
	}

	public void setSaturatedFat(double saturatedFat) {
		this.saturatedFat = saturatedFat;
	}

	public double getSugars() {
		return sugars;
	}

	public void setSugars(double sugars) {
		this.sugars = sugars;
	}

	public double getSalt() {
		return salt;
	}

	public void setSalt(double salt) {
		this.salt = salt;
	}

	public double getFiber() {
		return fiber;
	}

	public void setFiber(double fiber) {
		this.fiber = fiber;
	}

	public double getProteins() {
		return proteins;
	}

	public void setProteins(double proteins) {
		this.proteins = proteins;
	}

	public int getNutritionScore() {
		return nutritionScore;
	}

	public void setNutritionScore(int nutritionScore) {
		this.nutritionScore = nutritionScore;
	}
}
