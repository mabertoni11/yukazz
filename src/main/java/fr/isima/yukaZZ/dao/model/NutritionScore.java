package fr.isima.yukaZZ.dao.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * The Model of a Nutrition Score object
 *
 */
@Entity
@Table(name="nutrition_score")
public class NutritionScore {
	
	/**
	 * The Id of the nutrition score
	 */
	@Id
	@Column(name="Id", nullable = false)
	private int Id;
	
	/**
	 * The Classe of the nutrition score
	 */
	@Column(name="classe", nullable = false)
	private String Classe;
	
	/**
	 * The lower bound of the nutrition score
	 */
	@Column(name="lower_bound", nullable = false)
	private int LowerBound;
	
	/**
	 * The Upper bound of the nutrition score
	 */
	@Column(name="upper_bound", nullable = false)
	private int UpperBound;
	
	/**
	 * The color of the nutrition score
	 */
	@Column(name="color", nullable = false)
	private String Color;
	
	/**
	 * The Public constructor of the nutrition score
	 */
	public NutritionScore() {
		
	}

	public int getId() {
		return Id;
	}

	public void setId(int id) {
		Id = id;
	}

	public String getClasse() {
		return Classe;
	}

	public void setClasse(String classe) {
		Classe = classe;
	}

	public int getLowerBound() {
		return LowerBound;
	}

	public void setLowerBound(int lowerBound) {
		LowerBound = lowerBound;
	}

	public int getUpperBound() {
		return UpperBound;
	}

	public void setUpperBound(int upperBound) {
		UpperBound = upperBound;
	}

	public String getColor() {
		return Color;
	}

	public void setColor(String color) {
		Color = color;
	}
}