package fr.isima.yukaZZ.dao.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * The Model of a Rule object
 *
 */
@Entity
@Table(name="rule")
public class Rule {
	
	/**
	 * The Id of the rule
	 */
	@Id
	@Column(name="id", nullable = false)
	private int Id;
	
	/**
	 * The Name of the rule
	 */
	@Column(name="name", nullable = false)
	private String Name;
	
	/**
	 * The Points of the rule
	 */
	@Column(name="points", nullable = false)
	private int Points;
	
	/**
	 * The Min Bound of the rule
	 */
	@Column(name="min_bound", nullable = false)
	private double MinBound;
	
	/**
	 * The component Type of the rule 'P' or 'N'
	 */
	@Column(name="component", nullable = false)
	private char Type;
	
	/**
	 * The public constructor of a rule
	 */
	public Rule() {
		
	}

	public int getId() {
		return Id;
	}

	public String getName() {
		return Name;
	}

	public int getPoints() {
		return Points;
	}

	public double getMinBound() {
		return MinBound;
	}

	public char getType() {
		return Type;
	}
}
