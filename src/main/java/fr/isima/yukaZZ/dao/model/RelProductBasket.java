package fr.isima.yukaZZ.dao.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Class to recover information about the connection between product and basket of the database
 */

@Entity
@Table(name = "REL_PRODUCT_BASKET")
public class RelProductBasket {
	
	@Id
	@Column(name = "REL_ID", nullable = false)
	private long id;
	
	@Column(name="REL_PROD_ID", nullable = false)
	private long productId;
	
	@Column(name="REL_BASK_ID", nullable = false)
	private long basketId;
	
	@Column(name="REL_QUANTITY", nullable = false)
	private int quantity;
	
	public RelProductBasket(long id, long productId, long basketId) {
		this.id = id;
		this.productId = productId;
		this.basketId = basketId;
		this.quantity = 0;
	}
	
	public long getId() {
		return id;
	}
	
	public long getQuantity() {
		return quantity;
	}
	
	public void addQuantity() {
		quantity++;
	}
	
	public void removeQuantity() {
		quantity--;
	}
	
	public RelProductBasket() {}
	
	public long getProductId() {
		return productId;
	}
	
}
