package fr.isima.yukaZZ.dao.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * The Model of Component objects
 *
 */
@Entity
@Table(name = "COMPONENT")
public class Component {

	/**
	 * The Id of the Component
	 */
	@Id
	@Column(name = "COMP_ID", nullable = false)
	private long id;

	/**
	 * The Id of the Product where the component is
	 */
	@Column(name = "COMP_PRODID", nullable = false)
	private long productId;

	/**
	 * The component Name
	 */
	@Column(name = "COMP_NAME", nullable = false)
	private String name;

	/**
	 * The component type 'N' or 'P'
	 */
	@Column(name = "COMP_TYPE", nullable = false)
	private char type;

	/**
	 * The content of the component in the product
	 */
	@Column(name = "COMP_VALUE", nullable = false)
	private double value;

	/**
	 * The quality of the component (défaut, qualité or neutre)
	 */
	@Column(name = "COMP_QUALITY", nullable = false)
	private String quality;

	/**
	 * The Points of the component
	 */
	@Column(name = "COMP_POINTS", nullable = false)
	private int points;

	/**
	 * Public Constructor of a component
	 * 
	 * @param productId
	 * 			the product's id
	 * @param name
	 * 			the component's name
	 * @param type
	 * 			the component's type
	 * @param value
	 * 			the component's value
	 */
	public Component(long productId, String name, char type, double value) {
		this.productId = productId;
		this.name = name;
		this.type = type;
		this.value = value;
	}

	/**
	 * Constructor with all value
	 * 
	 * @param id
	 * 			the component's id
	 * @param productId
	 * 			the product's id
	 * @param name
	 * 			the component's name
	 * @param type
	 * 			the component's type
	 * @param value
	 * 			the component's value
	 * @param quality	
	 * 			the component's quality
	 * @param points
	 * 			the component's points
	 */
	public Component(long id, long productId,String name,char type,double value, String quality,int points) {
		this.id = id;
		this.productId = productId;
		this.name = name;
		this.type = type;
		this.value = value;
		this.quality = quality;
		this.points = points;
	}
	
	/**
	 * Default constructor
	 */
	public Component() {
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public char getType() {
		return type;
	}

	public void setType(char type) {
		this.type = type;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	public String getQuality() {
		return quality;
	}

	public void setQuality(String quality) {
		this.quality = quality;
	}

	public int getPoints() {
		return points;
	}

	public void setPoints(int points) {
		this.points = points;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getProductId() {
		return productId;
	}

	public void setProductId(long productId) {
		this.productId = productId;
	}
}
