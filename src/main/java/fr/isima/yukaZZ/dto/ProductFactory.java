package fr.isima.yukaZZ.dto;

import fr.isima.yukaZZ.dao.model.Component;
import fr.isima.yukaZZ.dao.model.Product;

/**
 * The Factory class to transform object between Product and ProductDto 
 *
 */
public class ProductFactory {
	
	/**
	 * Transforms a Product entity into a ProductDto
	 * @param entity the entity to transform
	 * @return The ProductDto corresponding to the input entity
	 */
	public static ProductDto ProductEntityToDto(Product entity) {
		ProductDto dto = new ProductDto();
		dto.setBarCode(entity.getBarCode());
		dto.setId(entity.getId());
		dto.setName(entity.getName());
		dto.setNutritionScore(entity.getNutritionScore());
		
		dto.AddComponent(new Component(entity.getId(),"energy_100g",'N',entity.getEnergyDensity()));
		dto.AddComponent(new Component(entity.getId(),"saturated-fat_100g", 'N', entity.getSaturatedFat()));
		dto.AddComponent(new Component(entity.getId(),"sugars_100g",'N',entity.getSugars()));
		dto.AddComponent(new Component(entity.getId(),"salt_100g",'N',entity.getSalt()));
		dto.AddComponent(new Component(entity.getId(),"fiber_100g",'P',entity.getFiber()));
		dto.AddComponent(new Component(entity.getId(),"proteins_100g",'P',entity.getProteins()));

		return dto;
	}
	
	/**
	 * Transforms a ProductDto into a Product entity
	 * @param dto the Product Dto to transform
	 * @return The product entity corresponding to the input ProductDto
	 */
	public static Product ProductDtoToEntity(ProductDto dto) {
		Product entity= new Product();
		entity.setBarCode(dto.getBarCode());
		entity.setId(dto.getId());
		entity.setName(dto.getName());
		entity.setNutritionScore(dto.getNutritionScore());
		
		entity.setEnergyDensity(dto.getComponents().get(0).getValue());
		entity.setSaturatedFat(dto.getComponents().get(1).getValue());
		entity.setSugars(dto.getComponents().get(2).getValue());
		entity.setSalt(dto.getComponents().get(3).getValue());
		entity.setFiber(dto.getComponents().get(4).getValue());
		entity.setProteins(dto.getComponents().get(5).getValue());
		
		return entity;
	}
}
