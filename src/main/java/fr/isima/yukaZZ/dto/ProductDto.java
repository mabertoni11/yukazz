package fr.isima.yukaZZ.dto;

import java.util.ArrayList;
import java.util.List;

import fr.isima.yukaZZ.dao.model.Component;

/**
 * The Model of Object result for Product
 *
 */
public class ProductDto {
	/**
	 * The id of the Product
	 */
	private long id;
	
	/**
	 * The Bar Code of the product
	 */
	private String barCode;
	
	/**
	 * The Name of the product
	 */
	private String name;
	
	/**
	 * The list of the Components contents in the product
	 */
	private List<Component> components;
	
	/**
	 * The Nutrition Score of the product
	 */
	private int nutritionScore;
	
	/**
	 * The quantity of the product
	 */
	private long quantity;
	
	/**
	 * Constructor
	 * 
	 * @param id
	 * 			the id
	 * @param barCode
	 * 			the bar code
	 * @param name
	 * 			the name
	 * @param components
	 * 			the components
	 * @param nutritionScore
	 * 			the nutrition score
	 * @param quantity
	 * 			the quantity
	 */
	public ProductDto(long id, String barCode, String name, List<Component> components, int nutritionScore, long quantity) {
		this.id = id;
		this.barCode = barCode;
		this.name = name;
		this.components = components;
		this.nutritionScore = nutritionScore;
		this.quantity = quantity;
	}
	
	
	/**
	 * The public constructor of a productDto
	 */
	public ProductDto() {
		this.components = new ArrayList<Component>(6);
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}

	public void setBarCode(String barCode) {
		this.barCode = barCode;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<Component> getComponents() {
		return components;
	}
	public void setComponents(List<Component> Components) {
		this.components = Components;
	}	
	public int getNutritionScore() {
		return nutritionScore;
	}
	public void setNutritionScore(int nutritionScore) {
		this.nutritionScore = nutritionScore;
	}
	
	
	public void AddComponent(Component compo) {
		this.components.add(compo);
	}
	
	public void ChangeComponent(int index, Component compo) {
		this.components.set(index, compo);
	}
	
	public String getBarCode() {
		return barCode;
	}
	
	public long getQuantity() {
		return quantity;
	}
	
	public ProductDto(String barCode, long quantity) {
		this.barCode = barCode;
		this.quantity = quantity;
	}

	/**
	 * Convert the main informations of a product into a String
	 */
	@Override
	public String toString() {
		String ComponentsString = "";
		for(Component compo : this.components){
			ComponentsString += compo.getName() + " : "+ compo.getValue() + "\n";
		}
		return "Produit : " + this.name + "\n" +
				"Code barre : " + this.barCode + "\n" + ComponentsString;
	}
}
